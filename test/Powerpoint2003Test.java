import org.junit.Before;
import org.junit.Test;
import org.sgoffice.features.Imageable;
import org.sgoffice.office.powerpoint.PowerPoint2003;

import java.io.File;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by Ronald on 12/29/2014.
 */
public class Powerpoint2003Test {
    PowerPoint2003 ppt;
    String ppt2003FileName;
    String ppt2003SlideFolder;

    @Before
    public void setup(){
       ppt2003FileName = "test" +File.separator + "assets" + File.separator + "ppt2003TestFile.ppt";
       ppt2003SlideFolder = "test" +File.separator + "assets" + File.separator + "ppt2003SlideImages";
       ppt = new PowerPoint2003(ppt2003FileName);
    }

    @Test
    public void powerpointFileIsCreated(){
        File pptFile = ppt.getFile();
        assertThat(pptFile, is(new File(ppt2003FileName)));
    }

    @Test
    public void powerpointObjectGeneratesImages(){
        Imageable ppt = new PowerPoint2003(ppt2003FileName);
        ppt.generateSlideImagesToFolder(ppt2003SlideFolder);
        int numberOfSlides = Utils.countFilesInDir(ppt2003SlideFolder);
        assertThat(numberOfSlides, is(((PowerPoint2003)ppt).getNumberOfSlides()));
    }

}
