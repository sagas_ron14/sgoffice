import java.io.File;

/**
 * Created by Ronald on 12/29/2014.
 */
public class Utils {
    public static int countFilesInDir(String directoryName){
        File folder = new File(directoryName);

        if(folder.exists() && folder.isDirectory()){
            File[] files = folder.listFiles();
            if(files != null)
                return files.length;
        }

        return 0;
    }
}
