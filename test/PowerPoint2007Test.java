import org.junit.Before;
import org.junit.Test;
import org.sgoffice.features.Imageable;
import org.sgoffice.office.powerpoint.PowerPoint2007;

import java.io.File;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by Ronald on 12/29/2014.
 */
public class PowerPoint2007Test {
    PowerPoint2007 ppt;
    String ppt2007FileName;
    private String ppt2007SlideFolder;

    @Before
    public void setup(){
        ppt2007FileName = "test" +File.separator + "assets" + File.separator + "ppt2007TestFile.pptx";
        ppt2007SlideFolder = "test" +File.separator + "assets" + File.separator + "ppt2007SlideImages";
        ppt = new PowerPoint2007(ppt2007FileName);
    }

    @Test
    public void powerpointFileIsCreated(){
        File pptFile = ppt.getFile();
        assertThat(pptFile, is(new File(ppt2007FileName)));
    }

    @Test
    public void powerpointObjectGeneratesImages(){
        Imageable ppt = new PowerPoint2007(ppt2007FileName);
        ppt.generateSlideImagesToFolder(ppt2007SlideFolder);
        int numberOfSlides = Utils.countFilesInDir(ppt2007SlideFolder);
        assertThat(numberOfSlides, is(((PowerPoint2007)ppt).getNumberOfSlides()));
    }
}
