import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Test;
import org.sgoffice.features.Imageable;
import org.sgoffice.office.PowerPoint;
import org.sgoffice.office.powerpoint.PowerPointFormat;

import java.io.File;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by Ronald on 12/29/2014.
 */
public class PowerPointTest {
    String ppt2003FileName;
    String ppt2007FileName;
    PowerPoint powerpoint;
    private String ppt2003SlideFolder;
    private String ppt2007SlideFolder;

    @Before
    public void setup(){
        ppt2003FileName = "test" +File.separator + "assets" + File.separator + "ppt2003TestFile.ppt";
        ppt2007FileName = "test" +File.separator + "assets" + File.separator + "ppt2007TestFile.pptx";
        ppt2003SlideFolder = "test" +File.separator + "assets" + File.separator + "powerPoint2003SlideImages";
        ppt2007SlideFolder = "test" +File.separator + "assets" + File.separator + "powerPoint2007SlideImages";
    }

    @Test
    public void powerPointKnowsPPTFormatFromFileName(){
        powerpoint = new PowerPoint(ppt2003FileName);
        assertThat(powerpoint.getFormat(), is(PowerPointFormat.PPT));
    }

    @Test
    public void powerPointKnowsPPTXFormatFromFileName(){
        powerpoint = new PowerPoint(ppt2007FileName);
        assertThat(powerpoint.getFormat(), is(PowerPointFormat.PPTX));
    }

    @Test
    public void powerpointObjectGeneratesPPTImages(){
        Imageable ppt = new PowerPoint(ppt2003FileName);
        ppt.generateSlideImagesToFolder(ppt2003SlideFolder);
        int numberOfSlides = Utils.countFilesInDir(ppt2003SlideFolder);
        MatcherAssert.assertThat(numberOfSlides, is(((PowerPoint)ppt).getNumberOfSlides()));
    }

    @Test
    public void powerpointObjectGeneratesPPTXImages(){
        Imageable ppt = new PowerPoint(ppt2007FileName);
        ppt.generateSlideImagesToFolder(ppt2007SlideFolder);
        int numberOfSlides = Utils.countFilesInDir(ppt2007SlideFolder);
        MatcherAssert.assertThat(numberOfSlides, is(((PowerPoint)ppt).getNumberOfSlides()));
    }
}
