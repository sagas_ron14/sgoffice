package org.sgoffice.features;

/**
 * Created by Ronald on 12/29/2014.
 */
public interface Imageable {
    public void generateSlideImagesToFolder(String destinationDirectory);
}
