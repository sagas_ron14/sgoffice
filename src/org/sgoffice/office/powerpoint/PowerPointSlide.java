package org.sgoffice.office.powerpoint;

import org.apache.poi.hslf.model.Slide;
import org.apache.poi.xslf.usermodel.XSLFSlide;

import java.awt.*;

/**
 * Created by Ronald on 12/29/2014.
 */
public class PowerPointSlide implements Slideable {
    PowerPointFormat format;
    Object slide;

    public PowerPointSlide(Object slide, PowerPointFormat format) {
        this.format = format;
        this.slide = slide;
    }

    @Override
    public void draw(Graphics2D graphics) {
        switch (format){
            case PPT:
                ((Slide)slide).draw(graphics);
                break;
            case PPTX:
                ((XSLFSlide)slide).draw(graphics);
                break;
        }
    }
}
