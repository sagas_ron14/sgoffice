package org.sgoffice.office.powerpoint;

import java.awt.*;

/**
 * Created by Ronald on 12/29/2014.
 */
public interface SlideShowable {
    public Slideable[] getSlides();
    public Dimension getPageSize();
}
