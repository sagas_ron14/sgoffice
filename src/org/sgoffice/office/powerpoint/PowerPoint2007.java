package org.sgoffice.office.powerpoint;

import org.sgoffice.office.PowerPoint;
import org.sgoffice.office.powerpoint.PowerPointFormat;

/**
 * Created by Ronald on 12/29/2014.
 */
public class PowerPoint2007 extends PowerPoint {

    public PowerPoint2007(String ppt2007FileName) {
        super(ppt2007FileName, PowerPointFormat.PPTX);
    }
}
