package org.sgoffice.office.powerpoint;

/**
 * Created by Ronald on 12/29/2014.
 */
public enum PowerPointFormat {
    PPT, PPTX
}
