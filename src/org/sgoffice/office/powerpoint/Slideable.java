package org.sgoffice.office.powerpoint;

import java.awt.*;

/**
 * Created by Ronald on 12/29/2014.
 */
public interface Slideable {
    public void draw(Graphics2D graphics);
}
