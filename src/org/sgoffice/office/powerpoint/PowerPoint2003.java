package org.sgoffice.office.powerpoint;

import org.sgoffice.office.PowerPoint;
import org.sgoffice.office.powerpoint.PowerPointFormat;

/**
 * Created by Ronald on 12/29/2014.
 */
public class PowerPoint2003 extends PowerPoint {

    public PowerPoint2003(String ppt2003FileName){
        super(ppt2003FileName, PowerPointFormat.PPT);
    }

}
