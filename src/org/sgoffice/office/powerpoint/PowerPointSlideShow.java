package org.sgoffice.office.powerpoint;

import org.apache.poi.hslf.model.Slide;
import org.apache.poi.hslf.usermodel.SlideShow;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;

import java.awt.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Ronald on 12/29/2014.
 */
public class PowerPointSlideShow implements SlideShowable{

    private Object powerPoint;
    private PowerPointFormat format;
    private Slideable[] slides;

    public PowerPointSlideShow(PowerPointFormat format, FileInputStream inputStream)throws IOException{
        this.format = format;
        switch(format){
            case PPT:
                powerPoint = new SlideShow(inputStream);
                break;
            case PPTX:
                powerPoint = new XMLSlideShow(inputStream);
                break;
        }
    }

    @Override
    public Slideable[] getSlides() {
        ArrayList<Slideable> slydes = new ArrayList<Slideable>();
        switch (format){
            case PPT:
                for(Slide slide: ((SlideShow)powerPoint).getSlides())
                    slydes.add(new PowerPointSlide(slide, format));
                break;
            case PPTX:
                for(XSLFSlide slide: ((XMLSlideShow)powerPoint).getSlides())
                    slydes.add(new PowerPointSlide(slide, format));
                break;
        }

        slides = new Slideable[slydes.size()];
        for(int i=0; i<slydes.size(); i++)
            slides[i]=slydes.get(i);

        return slides;

    }

    @Override
    public Dimension getPageSize() {
        switch (format){
            case PPT:
                return ((SlideShow)powerPoint).getPageSize();
            case PPTX:
                return ((XMLSlideShow)powerPoint).getPageSize();
        }
        return null;
    }

}
