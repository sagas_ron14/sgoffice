package org.sgoffice.office;

import org.sgoffice.features.Imageable;
import org.sgoffice.office.powerpoint.PowerPointFormat;
import org.sgoffice.office.powerpoint.PowerPointSlideShow;
import org.sgoffice.office.powerpoint.SlideShowable;
import org.sgoffice.office.powerpoint.Slideable;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;

/**
 * Created by Ronald on 12/29/2014.
 */
public class PowerPoint implements Imageable{

    protected final File file;
    protected FileInputStream inputStream;
    protected FileOutputStream outputStream;
    private SlideShowable powerpoint;
    private Slideable[] slides;
    private PowerPointFormat format;

    public PowerPoint(String fileName){
        file = new File(fileName);
        setFormat();
    }

    public PowerPoint(String fileName, PowerPointFormat format){
        file = new File(fileName);
        this.format = format;
    }

    public int getNumberOfSlides(){
        if(slides == null){
            setSlideShow();
            slides = powerpoint.getSlides();
        }

        return slides.length;
    }

    public File getFile() {
        return file;
    }

    private void writeSlidesToDir(Slideable[] slides, String destinationDir, Dimension dimension){

        for (int i = 0; i < slides.length; i++) {
            BufferedImage img = new BufferedImage(dimension.width, dimension.height, BufferedImage.TYPE_INT_RGB);
            Graphics2D graphics = img.createGraphics();

            //clear the drawing area
            graphics.setPaint(Color.white);
            graphics.fill(new Rectangle2D.Float(0, 0, dimension.width, dimension.height));

            //render
            slides[i].draw(graphics);

            //save the output
            openFileOutputStream(destinationDir + File.separator + "slide-"  + (i+1) + ".png");
            try {
                javax.imageio.ImageIO.write(img, "png", outputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            closeOutputStream();
        }
    }

    private void closeOutputStream() {
        try {
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void openFileOutputStream(String path) {
        try {
            outputStream = new FileOutputStream(path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void setSlideShow(){
        if(powerpoint == null){
            openInputStream();
            if(inputStream != null){
                try {
                    powerpoint = new PowerPointSlideShow(format, inputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                closeInputStream();
            }
        }
    }

    private void openInputStream(){
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void closeInputStream(){
        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected boolean createDirectory(String destinationDirectory) {
        File slideDir = new File(destinationDirectory);
        if(slideDir.exists())
            slideDir.delete();
        return slideDir.mkdir();
    }

    @Override
    public void generateSlideImagesToFolder(String destinationDirectory) {
        if(createDirectory(destinationDirectory)){
            setSlideShow();
            Dimension dimension = powerpoint.getPageSize();
            slides = powerpoint.getSlides();
            writeSlidesToDir(slides, destinationDirectory, dimension);
        }
    }

    public PowerPointFormat getFormat() {
        return format;
    }

    private void setFormat(){
        String contentType = "";
        try {
            contentType = Files.probeContentType(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (contentType.equalsIgnoreCase("application/vnd.ms-powerpoint"))
            format = PowerPointFormat.PPT;
        else if(contentType.equalsIgnoreCase("application/vnd.openxmlformats-officedocument.presentationml.presentation"))
            format = PowerPointFormat.PPTX;

    }
}
